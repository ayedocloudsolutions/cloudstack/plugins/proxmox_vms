provider "proxmox" {
  pm_api_url      = var.pm_api_url
  pm_user         = var.pm_user
  pm_password     = var.pm_password
  pm_tls_insecure = true
}

# Shared tags
locals {
  # Common tags to be assigned to all resources
  common_tags = {
    "release"  = var.stack
    "provider" = "aki"
  }

  all_tags = merge(local.common_tags)
}

resource "proxmox_vm_qemu" "master" {
  count       = var.master_count
  name        = join("-", [var.stack, "master", count.index])
  target_node = var.target_node
  clone       = var.clone
  os_type     = "cloud-init"
  cores       = var.master_cores
  sockets     = var.master_sockets
  cpu         = "kvm64"
  memory      = var.master_memory
  scsihw      = "virtio-scsi-pci"
  bootdisk    = "scsi0"
  agent       = 1
  disk {
    size    = var.master_disk
    type    = "scsi"
    storage = var.master_storage
  }
  lifecycle {
    ignore_changes = [
      network,
    ]
  }
  network {
    model  = var.master_network_model
    bridge = var.master_network_bridge
  }
  ipconfig0 = var.master_ipconfig[count.index]
  ssh_user  = var.ssh_user
  sshkeys   = <<EOF
  ${file(var.ssh_public_key)}
  EOF
}

resource "proxmox_vm_qemu" "node" {
  count       = var.node_count
  name        = join("-", [var.stack, "node", count.index])
  target_node = var.target_node
  clone       = var.clone
  os_type     = "cloud-init"
  cores       = var.node_cores
  sockets     = var.node_sockets
  cpu         = "kvm64"
  memory      = var.node_memory
  scsihw      = "virtio-scsi-pci"
  bootdisk    = "scsi0"
  agent       = 1
  disk {
    size    = var.node_disk
    type    = "scsi"
    storage = var.node_storage
  }
  network {
    model  = var.node_network_model
    bridge = var.node_network_bridge
  }
  ipconfig0  = var.node_ipconfig[count.index]
  ssh_user   = var.ssh_user
  cipassword = var.password
  ciuser     = var.user
  sshkeys    = <<EOF
  ${file(var.ssh_public_key)}
  EOF
}

output "inventory" {
  value     = templatefile("${path.module}/templates/inventory.tpl", { ssh_user = var.ssh_user, masters = proxmox_vm_qemu.master, nodes = proxmox_vm_qemu.node })
  sensitive = true
}

resource "local_file" "inventory" {
  content  = templatefile("${path.module}/templates/inventory.tpl", { ssh_user = var.ssh_user, masters = proxmox_vm_qemu.master, nodes = proxmox_vm_qemu.node })
  filename = var.inventory_file
}

# output "Master-IPS" {
#   value = ["${proxmox_vm_qemu.master.*.default_ipv4_address}"]
# }
# output "worker-IPS" {
#   value = ["${proxmox_vm_qemu.node.*.default_ipv4_address}"]
# }
