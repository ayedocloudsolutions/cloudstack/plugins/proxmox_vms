variable "stack" {}

variable "ssh_public_key" {
  default = "~/.ssh/id_rsa.pub"
}

variable "inventory_file" {
  default = "/context/inventory.yml"
}


variable "pm_user" {
  default = "root@pam"
}
variable "pm_api_url" {
}
variable "pm_password" {
}
variable "pm_api_token_id" {
  default = ""
}
variable "pm_api_token_secret" {
  default = ""
}
variable "target_node" {}
variable "clone" {}
variable "ssh_user" {
  default = "cloudstack"
}
variable "user" {
  default = "cloudstack"
}
variable "password" {
  default = ""
}

variable "master_count" {
  default = 1
}

variable "master_cores" {
  default = 4
}
variable "master_sockets" {
  default = 1
}
variable "master_memory" {
  default = 4096
}
variable "master_disk" {
  default = 150
}

variable "master_storage" {
  default = "local-zfs"
}
variable "master_network_model" {
  default = "virtio"
}
variable "master_network_bridge" {
  default = "vmbr0"
}
variable "master_ipconfig" {
  type = list(string)
  default = [
    "dhcp"
  ]
}

variable "node_count" {
  default = 0
}

variable "node_cores" {
  default = 4
}
variable "node_sockets" {
  default = 1
}
variable "node_memory" {
  default = 4096
}
variable "node_disk" {
  default = 150
}

variable "node_storage" {
  default = "local-zfs"
}
variable "node_network_model" {
  default = "virtio"
}
variable "node_network_bridge" {
  default = "vmbr0"
}
variable "node_ipconfig" {
  default = []
}
