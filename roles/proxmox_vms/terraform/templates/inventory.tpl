all:
  hosts:
%{ for master in masters ~}
    ${master.name}:
      ansible_host: ${master.default_ipv4_address}
      ansible_python_interpreter: "/usr/bin/python3"
      ansible_user: ${ssh_user}
%{ endfor ~}
%{ for node in nodes ~}
    ${node.name}:
      ansible_host: ${node.default_ipv4_address}
      ansible_python_interpreter: "/usr/bin/python3"
      ansible_user: ${ssh_user}
%{ endfor ~}
  children:
    master:
      hosts:
%{ for master in masters ~}
        ${master.name}:
%{ endfor ~}
    node:
      hosts:
%{ for node in nodes ~}
        ${node.name}:
%{ endfor ~}
    k3s_cluster:
      children:
        master:
        node:
